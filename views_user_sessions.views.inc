<?php

/**
 * Implements hook_views_data().
 */
function views_user_sessions_views_data() {
  $data = array();

  // ----------------------------------------------------------------
  // sessions table

  $data['sessions']['table']['group']  = t('Sessions');

  // Advertise this table as a possible base table
  $data['sessions']['table']['base'] = array(
    'field' => 'uid',
    'title' => t('Sessions'),
    'help' => t('Stores site sessions information.'),
    'weight' => 10,
  );

  // For other base tables, explain how we join
  $data['sessions']['table']['join'] = array(
    'users' => array(
      'field' => 'uid',
      'left_field' => 'uid',
     ),
  );

  // aid
  $data['sessions']['uid'] = array(
    'title' => t('Uid'),
    'help' => t('User ID.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
    ),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // sid
  $data['sessions']['sid'] = array(
    'title' => t('Sid'),
    'help' => t('Session ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // ssid
  $data['sessions']['ssid'] = array(
    'title' => t('Ssid'),
    'help' => t('Secure session ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // hostname
  $data['sessions']['hostname'] = array(
    'title' => t('Hostname'),
    'help' => t('User hostname.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // timestamp
  $data['sessions']['timestamp'] = array(
    'title' => t('Timestamp'),
    'help' => t('The Unix timestamp when this session last requested a page.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // cache
  $data['sessions']['cache'] = array(
    'title' => t('Cache'),
    'help' => t('The time of this user’s last post.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}
